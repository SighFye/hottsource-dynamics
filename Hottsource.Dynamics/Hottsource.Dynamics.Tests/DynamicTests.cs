﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Hottsource.Dynamics.Tests
{
    [TestClass]
    public class DynamicTests
    {
        [TestMethod]
        public void CanProcessStringAsObject()
        {
            string test = "#ID\r\n1\r\n#Condition\r\n$TONETWORK$ != $FROMNETWORK$\r\n#Prompt\r\nThis is the prompt text\r\n#Hint\r\nThis is the hint text\r\n\r\n#ID\r\n2\r\n#Condition\r\n#Prompt\r\nThis is another prompt text\r\nThis is another line in the prompt\r\n#Hint\r\nThis is another hint text";
            List<Dictionary<string, string>> results = DynamicText.ProcessStringAsObject(test);
            Assert.IsTrue(results.Count == 2, "The results were not parsed correctly");
            Assert.IsTrue(results[0].Count == 4, "The results were not parsed correctly");
            Assert.IsTrue(results[0]["ID"].Equals("1"), "The results were not parsed correctly");
        }

        [TestMethod]
        public void CanProcessReplacePlaceholders()
        {
            string test = "This is $PLACEHOLDER$. I like $PH$";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                {"$PLACEHOLDER$", "AWESOME!"},
                {"$PH$", "doing this!"}
            };
            string result = DynamicText.ConvertPlaceholders(test, values);
            Assert.IsTrue(result.Equals("This is AWESOME!. I like doing this!"), "The placeholders were not converted correctly");
        }

        [TestMethod]
        public void CanProcessConditionalText_Equals_1Placeholder_True()
        {
            string test = "This condition is [$PH$=True|True|False].";
            Dictionary<string, string> values = new Dictionary<string,string>()
            {
                {"$PH$", "True"}
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is True."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_Equals_1Placeholder_False()
        {
            string test = "This condition is [$PH$=True|True|False].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                {"$PH$", "False"}
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is False."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_Equals_2Placeholders_True()
        {
            string test = "This condition is [$PH$=$PH2$|True|False].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                {"$PH$", "True"},
                {"$PH2$", "True"}
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is True."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_Equals_2Placeholders_False()
        {
            string test = "This condition is [$PH$=$PH2$|True|False].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                {"$PH$", "True"},
                {"$PH2$", "False"}
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is False."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_NotEquals_1Placeholder_True()
        {
            string test = "This condition is [$PH$!=True|True|False].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                {"$PH$", "False"}
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is True."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_NotEquals_1Placeholder_False()
        {
            string test = "This condition is [$PH$!=True|True|False].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                {"$PH$", "True"}
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is False."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_NotEquals_2Placeholders_True()
        {
            string test = "This condition is [$PH$!=$PH2$|True|False].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                {"$PH$", "True"},
                {"$PH2$", "False"}
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is True."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_NotEquals_2Placeholders_False()
        {
            string test = "This condition is [$PH$!=$PH2$|True|False].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                {"$PH$", "True"},
                {"$PH2$", "True"}
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is False."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_PlaceholderExists_True()
        {
            string test = "This condition is [$PH$|True|False].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                {"$PH$", "True"},
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is True."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_PlaceholderExists_False()
        {
            string test = "This condition is [$PH$|True|False].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {
                
            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is False."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanProcessConditionalText_NoFalseText()
        {
            string test = "This condition is [$PH$|True].";
            Dictionary<string, string> values = new Dictionary<string, string>()
            {

            };
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is ."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void ProcessConditionalTextIgnoresInvalidConditions()
        {
            string test = "This condition is [$PH$>True|Tets].";
            Dictionary<string, string> values = new Dictionary<string, string>();
            string result = DynamicText.ProcessConditionalText(test, values);
            Assert.IsTrue(result.Equals("This condition is [$PH$>True|Tets]."), "The conditional text did not work correctly");
        }
        [TestMethod]
        public void CanDetermineIfAStringIsACondition()
        {
            Assert.IsTrue(DynamicText.IsACondition("This=That").IsCondition, "Did not determine is what a condition");
            Assert.IsTrue(DynamicText.IsACondition("$This$").IsCondition, "Did not determine is what a condition");
            Assert.IsFalse(DynamicText.IsACondition("This is not a condition").IsCondition, "Did not determine is what a condition");
        }
        [TestMethod]
        public void CanGetCorrectTextWithMultipleConditions()
        {
            string test = "[Test=Test|Test!=Test|1|2|3]";
            string result = DynamicText.ProcessConditionalText(test, null);
            Assert.IsTrue(result.Equals("2"), "The conditional text did not work correctly");
        }
    }
}
