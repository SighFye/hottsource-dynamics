﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace Hottsource.Dynamics
{
    public static class DynamicText
    {
        public const string REGEX_CONDITIONALTEXT = "\\[(.*?)\\]";
        public const string REGEX_PLACEHOLDER = "\\$\\w*[A-Z]\\w*[A-Z]\\w*\\$";
        /// <summary>
        /// Takes in a string and converts it to a list of dictionaries representing objects and attributes
        /// </summary>
        /// <param name="input">String to be converted</param>
        /// <returns>List of dictionaries representing objects and attributes</returns>
        public static List<Dictionary<string,string>> ProcessStringAsObject(string input)
        {
            string[] lines = input.Split(
                new[] { "\r\n", "\r", "\n" },
                StringSplitOptions.None
            );
            List<Dictionary<string, string>> allObjects = new List<Dictionary<string, string>>();
            Dictionary<string, string> anObject = new Dictionary<string, string>();
            string key = "";
            string value = "";
            foreach (string line in lines)
            {
                if (line.StartsWith("#"))
                {
                    //New Attribute
                    if (!String.IsNullOrWhiteSpace(key))
                        anObject.Add(key, value);
                    value = "";
                    key = line.Replace("#", "");
                }
                else if (String.IsNullOrWhiteSpace(line))
                {
                    //New Object
                    anObject.Add(key, value);
                    value = "";
                    key = "";
                    allObjects.Add(anObject);
                    anObject = new Dictionary<string, string>();
                }
                else
                {
                    value += (!String.IsNullOrWhiteSpace(value) ? System.Environment.NewLine : "") + line;
                }
            }
            //Add the last attribute and object
            anObject.Add(key, value);
            allObjects.Add(anObject);
            return allObjects;
        }

        /// <summary>
        /// Processes Condition Text displaying the correct test required
        /// </summary>
        /// <param name="input">The input the test against</param>
        /// <param name="values">the placeholder values</param>
        /// <returns>The processed string</returns>
        public static string ProcessConditionalText(string input, Dictionary<string, string> values)
        {
            //Find all the conditional text
            MatchCollection mc = Regex.Matches(input, REGEX_CONDITIONALTEXT);
            foreach(Match m in mc)
            {
                string conditionalText = m.Value.Replace("[", "").Replace("]", "");
                List<string> conditionalParts = conditionalText.Split(new char[] {'|'}).ToList<string>();
                List<TextPart> testedTextParts = new List<TextPart>();
                foreach(string item in conditionalParts)
                {
                    TextPart textpart = IsACondition(item);
                    testedTextParts.Add(textpart);

                }
                List<TextPart> textPartIndexes = new List<TextPart>();
                for (int i = 0; i < testedTextParts.Count; i++)
                {
                    TextPart tPart = testedTextParts[i];
                    textPartIndexes.Add(tPart);
                    if (i == 0 && !tPart.IsCondition)
                    {
                        //If this is the first textpart, and it's not a condition, do not process any of this
                        break;
                    }
                    else if (tPart.IsCondition)
                    {
                        if (i + 1 <= testedTextParts.Count - 1)
                        {
                            tPart.IfTrue = testedTextParts[i + 1];
                            testedTextParts[i + 1].IsTrueResult = true;
                        }
                        testedTextParts[i + 1].Parent = tPart;
                    }
                    else
                    {
                        if (i + 1 <= testedTextParts.Count - 1)
                        {
                            if (tPart.IsTrueResult)
                            {
                                tPart.Parent.IfFalse = testedTextParts[i + 1];
                                testedTextParts[i + 1].Parent = tPart.Parent;
                            }
                            else
                            {
                                TextPart temp = tPart.Parent;
                                while(!temp.IsTrueResult)
                                {
                                    temp = temp.Parent;
                                }
                                temp.Parent.IfFalse = testedTextParts[i + 1];
                                testedTextParts[i + 1].Parent = temp.Parent;
                            }
                        }
                        if (i + 1 <= testedTextParts.Count - 1)
                        {
                            testedTextParts[i + 1].IsTrueResult = false;
                        }
                    }
                }
                TextPart aTextPart = testedTextParts[0];
                string replaceText = "";
                while (aTextPart.IsCondition)
                {
                    bool result = ProcessCondtion(aTextPart, values);
                    if(result)
                    {
                        aTextPart = aTextPart.IfTrue;
                    }
                    else
                    {
                        if (aTextPart.IfFalse != null)
                        {
                            aTextPart = aTextPart.IfFalse;
                        }
                        else
                        {
                            aTextPart = null;
                            break;
                        }
                    }
                }
                if (aTextPart != null)
                {
                    if (aTextPart.Parent == null && !aTextPart.IsCondition)
                    {
                        return input;
                    }
                    replaceText = aTextPart.OriginalText;
                }
                else
                {
                    
                }
                input = input.Replace(m.Value, replaceText);
            }
            return input;
        }

        public static TextPart IsACondition(string input)
        {
            TextPart tpart = new TextPart();
            tpart.OriginalText = input;
            //Find the operator
            if (input.Contains("!=")) tpart.Operator = "!=";
            else if (input.Contains("=")) tpart.Operator = "=";
            //else if(condition.Contains(">")) theOperator = ">";
            //else if(condition.Contains("<")) theOperator = "<";
            //else if(condition.Contains(">=")) theOperator = ">=";
            //else if(condition.Contains("<=")) theOperator = "<=";

            if (tpart.Operator != null)
            {
                string[] stringSeparators = new string[] { tpart.Operator };
                List<string> valuesToCompare = input.Split(stringSeparators, StringSplitOptions.None).ToList<string>();
                if(!String.IsNullOrWhiteSpace(valuesToCompare[0]) && !String.IsNullOrWhiteSpace(valuesToCompare[1]))
                {
                    tpart.Value1 = valuesToCompare[0];
                    tpart.Value2 = valuesToCompare[1];
                    tpart.IsCondition = true;
                }
                else
                {
                    tpart.IsCondition = false;
                    tpart.Operator = null;
                }
            }
            else
            {
                if(input.StartsWith("$") && input.EndsWith("$"))
                {
                    tpart.IsCondition = true;
                }
            }
            return tpart;
        }

        public static bool ProcessCondtion(TextPart tpart, Dictionary<string, string> values)
        {
            if (tpart.Operator != null)
            {
                string value1 = ConvertPlaceholders(tpart.Value1, values);
                string value2 = ConvertPlaceholders(tpart.Value2, values);
                switch (tpart.Operator)
                {
                    case "=":
                        if (value1.Equals(value2)) return true;
                        break;
                    case "!=":
                        if (!value1.Equals(value2)) return true;
                        break;
                }
            }
            else
            {
                if (values.ContainsKey(tpart.OriginalText)) return true;
            }
            return false;
        }
        /// <summary>
        /// Converts placeholers to their given value
        /// </summary>
        /// <param name="input">The input to edit</param>
        /// <param name="values">The placeholders values</param>
        /// <returns>The edited string</returns>
        public static string ConvertPlaceholders(string input, Dictionary<string, string> values)
        {
            foreach(Match m in Regex.Matches(input, REGEX_PLACEHOLDER))
            {
                if (values.ContainsKey(m.Value))
                {
                    input = input.Replace(m.Value, values[m.Value]);
                }
            }
            return input;
        }
    }

    public class TextPart
    {
        public bool IsTrueResult { get; set; }
        public TextPart Parent { get; set; }
        public TextPart IfTrue { get; set; }
        public TextPart IfFalse { get; set; }
        public bool IsCondition { get; set; }
        public string OriginalText { get; set; }
        public string Operator { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
    }
}
